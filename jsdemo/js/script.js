//封装一个代替getElementById()的方法
function byId(id) {
    return typeof(id) == "string" ? document.getElementById(id):id;
}

// 全局变量
var index = 0,
    timer = null,
    pics = byId("banner").getElementsByTagName("div"),
    dots = byId("dots").getElementsByTagName("span"),
    len = pics.length,
    prev = byId("prev"),
    next = byId("next"),
    menu=byId("menu").getElementsByClassName("menu-item"),
    subMenu=byId("sub-menu"),
    subMenuItems = subMenu.getElementsByClassName("inner-box"),
    menuLen = menu.length,
    menuIndex = 0;

function slideImg() {
    var main = byId("main");
    var banner = byId("banner");
    var menuContent = byId("menu");
    // 滑过清除定时器，离开继续
    main.onmouseover = function () {
        // 清除定时器
        if(timer) {
            clearInterval(timer);
        }
    };
    main.onmouseout = function () {
        timer = setInterval(function () {
            index++;
            if(index >= len) {
                index = 0;
            }
            changeImg();
        },2000);
    }
    // 自动触发鼠标离开事件
    main.onmouseout();

    // 点击圆点切换图片
    for(var d=0;d<len;d++) {
        // 给所有span添加一个id属性
        dots[d].id = d;
        dots[d].onclick = function () {
            index = this.id;
            changeImg();
        }
    }

    // 上一张
    prev.onclick = function () {
        index --;
        if(index<0) {
            index = len-1;
        }
        changeImg();
    }
    // 下一张
    next.onclick = function () {
        index ++;
        if(index>=len) {
            index = 0;
        }
        changeImg();
    }

    //菜单
    for (var i=0;i<menuLen;i++) {
        menu[i].setAttribute("data-index", i);
        menu[i].onmouseover = function (ev) {
            var idx = this.getAttribute("data-index");
            subMenu.className = "sub-menu";
            for(var j=0;j<menuLen;j++) {
                subMenuItems[j].style.display = "none";
                menu[j].style.background = "none";
            }
            subMenuItems[idx].style.display = "block";
            menu[idx].style.background = "rgba(0,0,0,0.1)";
        }
    }

    subMenu.onmouseover = function(){
        this.className = "sub-menu";
    }

    subMenu.onmouseout = function(){
        this.className = "sub-menu hide";
    }

    banner.onmouseout = function(){
        subMenu.className = "sub-menu hide";
    }

    menuContent.onmouseout = function(){
        subMenu.className = "sub-menu hide";
    }
}
function changeImg() {
    console.log(index);
    for (var i = 0; i < len; i++) {
        if(i==index) {
            pics[i].style.display = "block";
            dots[i].className = "active";
        }else {
            pics[i].style.display = "none";
            dots[i].className = "";
        }
    }
}


slideImg();